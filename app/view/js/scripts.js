function onClickShowBddButton(event){
     event.preventDefault();
     var name = $(this).text();
     
     $.ajax({
        url: 'app/controller/showTableController.php',
        type: 'POST',
        data: 'show=' + name,
        success: (function(data){
            $(".container-fluid").load('app/view/bdd.html', function(){
                $('.container-fluid > [name="bddName"]').attr('value',name);
                $('#delete-bdd').append(name);
                $('#rename-bdd').append(name);
                data = $.parseJSON(data);

                $.each(data, function(index,value) {
                    $("#tableau").ready(function() {
                        $("#tableau").append("<tr><th scope='row'>"+ (index+1) +"</th><td><a href=#>"+ value['tableName'] +"</a></td><td>"+ value['tableDate'] +"</td></tr>");
                    });
                });

                $('#tableau').ready(function (){
                    $('#nbTable').text("Table (" + data.length + ")" );
                });

                $('#tableau').ready(function (){
                    $.ajax({
                        url: 'app/controller/showTableController.php',
                        type: 'POST',
                        data: 'size=' + name,
                        success: (function(data){
                            data = $.parseJSON(data);
                            $('#tableau').ready(function (){
                                $('#nbTable').append("("+ (Math.round(data[0]['size']*100)/100) +"MB)");
                            })
                        })
                    });
                });
            });
        })
    });
 }

function onClickAddBddButton(event){
    event.preventDefault();

    $("#add-bdd-div > *").replaceWith("<input type=text id='inputAdd' /><button class='btn btn-success' id='create-bdd'>Creer</button>");
}

function onClickCreateBddButton(event){
    event.preventDefault();
    var bddName = $('.container-fluid input').val();

   event.preventDefault();

    $.ajax({
        url: 'app/controller/addBddController.php',
        type: 'POST',
        data: 'add=' + bddName,
        success: (function(data){
            $("#dialog-confirm-add").dialog({
                buttons: {
                    Ok: function(){
                        $(this).dialog( "close" );
                        return false;
                    }
                }
            });
            $('#inputAdd').replaceWith("<a href='#' class='btn btn-primary' id='add-bdd'>Add new bdd</a>");
            $('#create-bdd').remove();
            $('.sidebar-nav').append('<li><a href="#">'+ bddName +'</a></li>');
        })
     });
}

function onClickDeleteBddButton(event){
    event.preventDefault();
    var bddName = $('.container-fluid > [name="bddName"]').attr('value');

   $("#dialog-confirm-delete").dialog({
            buttons: {
                Delete: function() {
                    $.ajax({
                        url: 'app/controller/deleteBddController.php',
                        type: 'POST',
                        data: 'delete=' + bddName,
                        success: (function(data){
                            $('.sidebar-nav li').each(function(){ 
                                if($(this).text() == bddName)
                                    $(this).remove();
                            });
                            $('.container-fluid').load('app/view/index.phtml .container-fluid > *');
                        })
                     });
                    $(".ui-dialog").remove();
                },
                Cancel: function() {
                    $(this).dialog( "close" );
                    return false;
                }
            }
    });
}

function onclickRenameBddButton(event){
    event.preventDefault();
    var bddName = $('.container-fluid > [name="bddName"]').attr('value');

    $("#dialog-confirm-rename").dialog({
                buttons: {
                    Ok: function(){
                        $(this).dialog( "close" );
                        return false;
                    }
                }
            });
}

function onclickCreateTableBddButton(event){
    event.preventDefault();
    
    $(this).replaceWith("<input type=text id='inputAddTable' /><button class='btn btn-success' id='create-table'>add the table</button>");
}

function onclickAddTableBddButton(event){
    var i = 0;
    event.preventDefault();

    var bddName = $('.container-fluid > [name="bddName"]').attr('value');
    var tableName = $('#inputAddTable').val();
    $.ajax({
        url: 'app/controller/addTableController.php',
        type: 'POST',
        data: 'bdd=' + bddName + '&table=' + tableName,
        success: (function(data){
            while(i < $('.sidebar-nav li').length){
                if ($('.sidebar-nav li a')[i].innerHTML == bddName){
                    $('.sidebar-nav li a')[i].click();
                    return false;
                }
                i++;
            }
        })
     });
}

function onClickTable(event){
    event.preventDefault();
    var tableName = $(this).children('td:nth-of-type(1)').text();
    var bddName = $('.container-fluid > [name="bddName"]').attr('value');
    var i = 0;

    $.ajax({
        url: 'app/controller/showTableContentController.php',
        type: 'POST',
        data: 'tableName=' + tableName + '&bddName=' + bddName,
        success: (function(data){
            data = $.parseJSON(data);
            $("#page-content-wrapper > *").load('app/view/table.html', function(){
                $.each(data, function(index,value) {
                    $(".tabletable thead tr").append("<th scope='col'>"+ value +"</th>");
                    i++;
                });

                $.ajax({
                    url: 'app/controller/showTableContentController.php',
                    type: 'POST',
                    data: 'tableName=' + tableName + '&bddName=' + bddName + "&content=true",
                    success: (function(data){
                        data = $.parseJSON(data);
                        $('.tabletable tbody').ready(function(){
                           for(var x = 0; x < data.length ; x++){
                               $('.tabletable tbody').append('<tr>');
                               for(var y = 0; y < i ; y++){
                                   $('.tabletable tbody').append("<td>"+ data[x][y] +"</td>");
                               }
                               $('.tabletable tbody').append("<td id='delete-row'>X</td>")
                               $('.tabletable tbody').append('</tr>');
                           }
                          $('.container-fluid > [name="bddNameTwo"]').attr('value',bddName);  
                          $('.container-fluid > [name="tableName"]').attr('value',$(this).attr('href'));
                        });
                    })
                });    
            })
        })
    });
}



$(document).on('click', '#tableau tr' , onClickTable);
$(document).on('click', '#create-table', onclickAddTableBddButton);
$(document).on('click', '#add-table', onclickCreateTableBddButton);
$(document).on('click', '#rename-bdd', onclickRenameBddButton);
$(document).on('click', '#delete-bdd', onClickDeleteBddButton);
$(document).on('click', '#create-bdd', onClickCreateBddButton);
$(document).on('click', '#add-bdd' , onClickAddBddButton);
$(document).on('click', "#sidebar-wrapper ul li:nth-of-type(n+2) a" ,onClickShowBddButton);