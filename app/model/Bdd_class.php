<?php

    class Bdd
    {
        //----------------------------------------
        //SINGLETON
        //----------------------------------------
        private $BddServeur = "127.0.0.1";
        private $BddLogin = "root";             
        private $BddPassword = "root";
        private $connect;
 
        public function __construct()
        {
            $BddServeur = $this->BddServeur;
            $BddLogin = $this->BddLogin;
            $BddPassword = $this->BddPassword;
        }
 
        public function actionData($database,$request)
        {
            try{
                $this->connect = new PDO('mysql:host='.$this->BddServeur.';dbname='.$database,$this->BddLogin,$this->BddPassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
                $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $connect = $this->connect;
            }
            catch(Exception $e){
                echo "impossible de se connecter la base de données";
                die('Erreur : '.$e->getMessage());
            } 
            
            $query = $connect->query($request);
        }
        
        public function Data($database,$request){
            try{
                $this->connect = new PDO('mysql:host='.$this->BddServeur.';dbname='.$database,$this->BddLogin,$this->BddPassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
                $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $connect = $this->connect;
            }
            catch(Exception $e){
                echo "impossible de se connecter la base de données";
                die('Erreur : '.$e->getMessage());
            } 
            
            $query = $connect->query($request)->fetchAll();
            
            return $query;
        }
        
    }
?>
