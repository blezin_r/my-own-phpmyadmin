<?php

class mySQL {

    private $serveurname = "localhost";
    private $username = "root";
    private $mdp = "root";

    function __construct() {
        $serveurname = $this->serveurname;
        $username = $this->username;
        $mdp = $this->mdp;
    }

    public function connect() {
        $conn = mysqli_connect($this->serveurname, $this->username, $this->mdp);

        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
        
        return $conn;
    }
    
    public function Data($request) {
        
        $conn = $this->connect();
        
        if (mysqli_query($conn, $request)) {
            $result = mysqli_query($conn, $request)->fetch_all();
        } else {
            $result = "Error : " . mysqli_error($conn);
        }

        mysqli_close($conn);

        return $result;
    }

        public function actionData($request) {
        
        $conn = $this->connect();
        
        if (mysqli_query($conn, $request)) {
            $result = mysqli_query($conn, $request);
        } else {
            $result = "Error : " . mysqli_error($conn);
        }

        mysqli_close($conn);
    }

}
