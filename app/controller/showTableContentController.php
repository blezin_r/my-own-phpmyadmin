<?php

include_once "../model/mysql_class.php";
include_once "../model/Bdd_class.php";

$bdd = new Bdd();
$mysql = new mySQL();

if ($_POST['content'] ?? 0){

	$req = "SELECT * FROM ". $_POST['tableName'];
	$tableContent = $bdd->Data($_POST['bddName'],$req);
	echo json_encode($tableContent);

} else {
	$req = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = '" . $_POST['bddName'] ."' AND TABLE_NAME = '". $_POST['tableName'] ."'";

	$tableFields = $mysql->Data($req);
	echo json_encode($tableFields);
}