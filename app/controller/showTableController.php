<?php

include "../model/Bdd_class.php";

$bdd = new Bdd();

if (isset($_POST['show'])){
	$req = "SELECT TABLE_NAME AS 'tableName', DATE_FORMAT(CREATE_TIME, '%d/%m/%Y %Hh%imin%ss') AS 'tableDate' FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '" . $_POST['show'] . "'";

	$tables = $bdd->Data($_POST['show'],$req);


	echo json_encode($tables);
} else if (isset($_POST['size'])) {
	$req = "SELECT table_schema 'database', sum(data_length + index_length) / 1024 / 1024 AS size FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '" . $_POST['size'] . "'";
	$tables = $bdd->Data($_POST['size'],$req);
	echo json_encode($tables);
}