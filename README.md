#### Les aspects techniques du projet

 - La programmation objet avec PHP7

Vous devez impérativement utiliser le PHP7 et la programmation objet pour ce projet mais aussi PDO pour la communication à la base de données.

- Le MVC

Une architecture MVC (model, view, controller) est également obligatoire. Un début d'architecture vous est présenté dans le cour, vous pouvez donc vous en servir pour construire votre propre architecture avec pour contrainte de faire du MVC (les frameworks PHP sont donc STRICTEMENT INTERDIT).


#### Étape 1 - Affichage et gestions des bases de données

Vous devez lister les bases de données et proposer les actions suivantes :

-   Ajout/création d’une base de données
-   Edition du nom de la base de données (de manière actuelle s’il vous plait !)
-   Suppression de la base de données (avec message de confirmation)
-   Statistiques de la base de données (nombre de tables, date de création, espace mémoire)


#### Étape 2 - Affichage et gestions des tables d’une base de données

Vous devez lister les tables d’une base de données et proposer les actions suivantes :

-   Ajout / création d’une table
-   Renommage d’une table
-   Ajout d’un élément dans la structure de la table
-   Suppression d’un élément de la structure de la table (avec message de confirmation)
-   Edition d’un élément de structure de la table
-   Statistiques de la table (nombre de lignes)


#### Étape 3 - Affichage et gestion du contenu d’une table

Vous devez lister les lignes de la table et proposer les actions suivantes :

-   Ajout d’une ligne dans la table
-   Suppression d’une ligne dans la table (avec message de confirmation)
-   Edition d’une ligne de la table


#### Étape 4 - Requête libre

Vous devez proposer une zone contenant une textarea et permettant de faire des requêtes SQL de manière libre. Si l’utilisateur génère une requête avec erreur, vous devez remonter l’erreur.